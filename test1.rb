# frozen_string_literal: true

class Array
  def my_map
    result = []
    if block_given?
      each { |element| result << yield(element) }
    else
      result = to_enum :my_map
    end
    result
  end
end

p [1, 2, 3].map { |n| n * 2 }    # [2, 4, 6]
p [1, 2, 3].my_map { |n| n * 2 } # [2, 4, 6]
p [1, 2, 3].my_map
