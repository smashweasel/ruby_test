# frozen_string_literal: true

def fib(n)
  fib_hash = Hash.new { |hash, key| hash[key] = key < 2 ? key : hash[key - 1] + hash[key - 2] }

  fib_hash[n]
end

p fib(1) # 1 => 1
p fib(10) # 10 => 55
