# frozen_string_literal: true

list = %w[one two two three three three]

def count_identical_elements(ary)
  cache = Hash.new(0)

  ary.each do |element|
    cache[element] += 1
  end

  cache
end

p count_identical_elements(list) # => { "one" => 1, "two" => 2, "three" => 3 }
